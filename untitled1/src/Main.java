public class Main {
    public static void main(String[] args){
        AverageValue();
        ArrayValues();
        GreatestNumber();
    }
    //1. Найти среднее значение суммы чисел от 1 до 100

    public static void AverageValue() {
        int sum = 0;
        for(int i = 1; i <= 100; i++){
            sum += i;
        }
        double averageValue = (double) sum / 100;
        System.out.println(averageValue);
    }

    //2. Заполнить массив данными. К примеру такой int [] array = new int [10]

    public static void ArrayValues(){
        int [] array = new int [10];
        for(int i = 0; i < array.length; i++){
            array[i] = i;
            System.out.println(array[i]);
        }

    }

    //3. Найти максимальное число в массиве int [] array = {5,2,4,8,88,22,10}

    public static void GreatestNumber(){
        int [] numbers = {5,2,4,8,88,22,10};
        int greatestNumber = numbers[0];
        for(int i = 0; i < numbers.length; i++) {
            if(greatestNumber < numbers[i]) {
                greatestNumber = numbers[i];
            }
        }
        System.out.println(greatestNumber);
    }
}
